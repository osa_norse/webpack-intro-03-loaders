import $ from 'jquery';

const header = `
<nav class="navbar navbar-expand-lg navbar-light bg-light">
<a class="navbar-brand" href="#">Navbar</a>

<div class="collapse navbar-collapse">
  <ul class="navbar-nav mr-auto">
    <li class="nav-item active">
      <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="#">Link</a>
    </li>
    <li class="nav-item">
      <a class="nav-link disabled" href="#">Disabled</a>
    </li>
  </ul>
  <form class="form-inline my-2 my-lg-0">
    <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">
    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
  </form>
</div>
</nav>
`;

const body = `<p>Some Huge Content</p><p>Some Huge Content</p><p>Some Huge Content</p><p>Some Huge Content</p><p>Some Huge Content</p><p>Some Huge Content</p><p>Some Huge Content</p><p>Some Huge Content</p><p>Some Huge Content</p><p>Some Huge Content</p><p>Some Huge Content</p><p>Some Huge Content</p><p>Some Huge Content</p><p>Some Huge Content</p><p>Some Huge Content</p><p>Some Huge Content</p><p>Some Huge Content</p><p>Some Huge Content</p><p>Some Huge Content</p><p>Some Huge Content</p><p>Some Huge Content</p><p>Some Huge Content</p><p>Some Huge Content</p>`;

const footer = `<div class="card">
<div class="card-body">
  <h4 class="card-title">Footer</h4>
  <p class="card-text">Footer smaller text.</p>
</div>
</div>`;

$('#app').append(header);
$('#app').append(body);
$('#app').append(footer);